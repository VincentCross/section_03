// Copyright Vincent Cross 2019

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"

#include "Grabber.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	// Properties in Unreal editor
	UPROPERTY(EditAnywhere)
	bool GrabDebug = false;

	UPROPERTY(EditAnywhere)
	float GrabReach = 100;

	// Class properties
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* Input = nullptr;

	// Ray-cast and grab what's in reach
	void Grab();

	// Called when grab is released
	void Release();

	// Logs error if no physics component found
	void FindPhysicsHandleComponent();
	// Binds functions to input, logs error if no input component found
	void SetupInputComponent();

	FHitResult GetFirstPhysicsBodyFromLineTrace();
	FVector* GetPlayerEyesLocation();
	FRotator* GetPlayerEyesRotation();
	FVector GetLineTraceEnd();

	// Helper for GetPlayerEyesLocation/Rotation
	void UpdatePlayerEyesLocationRotation(FVector*, FRotator*);

	// Helper for GetFirstPhysicsBodyInReach
	void DrawGrabDebugLine(FVector*, FVector*);

	// Draws the line trace and returns first hit
	FHitResult GetFirstLineTraceHit(FVector*, FVector*);
};
