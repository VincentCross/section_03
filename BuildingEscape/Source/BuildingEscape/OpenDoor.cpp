// Copyright Vincent Cross 2019

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Containers/Array.h"
#include "UObject/Class.h"
#include "UObject/NoExportTypes.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find pressure plate trigger volume for %s"), *(GetOwner()->GetName()))
	}

	APlayerController* CurrentPlayer = GetWorld()->GetFirstPlayerController();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	// Open the door after weight is put on the trigger
	if (!bDoorOpen && (GetTotalMassOfActorsOnPlate() > MinPlateMass))
	{
		OnOpen.Broadcast();
		bDoorOpen = true;
	}

	// Close the door after weight is taken off the trigger
	if (bDoorOpen && (GetTotalMassOfActorsOnPlate() < MinPlateMass))
	{
		OnClose.Broadcast();
		bDoorOpen = false;			
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	/// Find all the overlapping actors
	TArray<AActor*> AllOverlappingActors;
	if (!PressurePlate) { return 0; }
	PressurePlate->GetOverlappingActors(OUT AllOverlappingActors);

	/// Iterate through them adding their masses
	float TotalMass = 0;
	for (const AActor* Actor : AllOverlappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return TotalMass;
}


