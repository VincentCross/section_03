// Copyright Vincent Cross 2019

#include "Grabber.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#include "Components/PrimitiveComponent.h"

#define OUT


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	/// Physics handle used to interact with objects with physics
	FindPhysicsHandleComponent();

	/// Bounding the ingame 'Grab' action to appropriate functions
	SetupInputComponent();
}

void UGrabber::FindPhysicsHandleComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find physics handle component for %s"), *(GetOwner()->GetName()))
	}
}
void UGrabber::SetupInputComponent()
{
	Input = GetOwner()->FindComponentByClass<UInputComponent>();
	if (Input)
	{
		/// Bind the inputs
		Input->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		Input->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find input component for %s"), *(GetOwner()->GetName()))
	}
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PhysicsHandle) { return; }
	/// If the physics handle is attached
	if (PhysicsHandle->GrabbedComponent)
	{
		/// Move object to end of line trace
		FVector LineTraceEnd = GetLineTraceEnd();
		PhysicsHandle->SetTargetLocation(LineTraceEnd);
	}
}

void UGrabber::Grab()
{
	/// Line trace and see if we reach any actors with physics body collision channel set
	auto HitResult = GetFirstPhysicsBodyFromLineTrace();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();
	
	if (!PhysicsHandle) { return; }
	/// If we hit something then attach a physics handle
	if (ActorHit)
	{
		/*PhysicsHandle->GrabComponentAtLocation(
			ComponentToGrab,
			NAME_None,
			ComponentToGrab->GetOwner()->GetActorLocation()
		);*/
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			ComponentToGrab,
			NAME_None,
			ComponentToGrab->GetOwner()->GetActorLocation(),
			FRotator(0, 0, 0)
		);
	}
}

void UGrabber::Release()
{
	if (!PhysicsHandle) { return; }
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->ReleaseComponent();
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyFromLineTrace()
{
	/// Get player view point and vector for line trace
	FVector LineTraceEnd = GetLineTraceEnd();
	FVector* PlayerLocation = GetPlayerEyesLocation();

	/// Display debug line to test line trace
	if (GrabDebug)
	{
		DrawGrabDebugLine(PlayerLocation, &LineTraceEnd);
	}

	/// Get the hit to be used to grab
	FHitResult Hit = GetFirstLineTraceHit(PlayerLocation, &LineTraceEnd);

	return Hit;
}

FVector * UGrabber::GetPlayerEyesLocation()
{
	FVector* PlayerLocation = new FVector;
	UpdatePlayerEyesLocationRotation(OUT PlayerLocation, OUT new FRotator);

	return PlayerLocation;
}

FRotator * UGrabber::GetPlayerEyesRotation()
{
	FRotator* PlayerRotation = new FRotator;
	UpdatePlayerEyesLocationRotation(OUT new FVector, OUT PlayerRotation);

	return PlayerRotation;
}

FVector UGrabber::GetLineTraceEnd()
{
	FVector* PlayerLocation = GetPlayerEyesLocation();
	FRotator* PlayerRotation = GetPlayerEyesRotation();

	return *PlayerLocation + (PlayerRotation->Vector() * GrabReach);
}

void UGrabber::UpdatePlayerEyesLocationRotation(FVector* PlayerLocation, FRotator* PlayerRotation)
{
	APlayerController* Player = GetWorld()->GetFirstPlayerController();
	Player->GetActorEyesViewPoint(OUT *PlayerLocation, OUT *PlayerRotation);
}

void UGrabber::DrawGrabDebugLine(FVector * PlayerLocation, FVector * LineTraceEnd)
{
	DrawDebugLine(
		GetWorld(),
		*PlayerLocation,
		*LineTraceEnd,
		FColor().Red,
		false,
		0,
		0,
		5
	);
}

FHitResult UGrabber::GetFirstLineTraceHit(FVector * PlayerLocation, FVector * LineTraceEnd)
{
	FHitResult Hit;
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		*PlayerLocation,
		*LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters
	);

	return Hit;
}
