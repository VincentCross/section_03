// Copyright Vincent Cross 2019

#include "OpenDoorDouble.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Containers/Array.h"


// Sets default values for this component's properties
UOpenDoorDouble::UOpenDoorDouble()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoorDouble::BeginPlay()
{
	Super::BeginPlay();

	if (!PressurePlateLeft || !PressurePlateRight)
	{
		UE_LOG(LogTemp, Error, TEXT("%s is missing a pressure plate."), *(GetOwner()->GetName()))
	}
	
}


// Called every frame
void UOpenDoorDouble::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Open the door after both plates are triggered
	if (!bDoorOpen && IsPlateActive(PressurePlateLeft) && IsPlateActive(PressurePlateRight))
	{
		OnOpen.Broadcast();
		bDoorOpen = true;
	}

	// Close the door when one or both plates are not triggered
	if (bDoorOpen && (!IsPlateActive(PressurePlateLeft) || !IsPlateActive(PressurePlateRight)))
	{
		OnClose.Broadcast();
		bDoorOpen = false;
	}
}

bool UOpenDoorDouble::IsPlateActive(ATriggerVolume* PressurePlate) const
{
	/// If there are actors on the plate then return true
	TArray<AActor*> AllOverlappingActors;
	if (!PressurePlate) { return 0; }
	PressurePlate->GetOverlappingActors(OUT AllOverlappingActors);

	if (AllOverlappingActors.Num() > 0)
		return true;
	
	return false;
}

